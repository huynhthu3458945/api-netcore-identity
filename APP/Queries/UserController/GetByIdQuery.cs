﻿using APP.DbContext.Models;
using MediatR;

namespace APP.WebApi.Queries.UserController
{
    public class GetByIdQuery : IRequest<Product>
    {
        public long ProductId { get; set; }
    }
}
