﻿using APP.DbContext.Models;
using MediatR;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace APP.WebApi.Models.Request
{
    public class ProductCommand : IRequest<Product>
    {
        public string ProductName { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> PriceCost { get; set; }
        public Nullable<decimal> PricePro { get; set; }
        public long BrandId { get; set; }
        public long CategoryId { get; set; }
    }
}
