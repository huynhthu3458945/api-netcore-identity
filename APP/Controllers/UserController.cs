﻿using APP.DbContext;
using APP.DbContext.Indentity;
using APP.DbContext.Models;
using APP.Services.Implements;
using APP.Services.Interfaces;
using APP.WebApi.Common;
using APP.WebApi.Handlers.UserController;
using APP.WebApi.Models.Request;
using APP.WebApi.Queries.UserController;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Net;

namespace APP.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator ;
        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        [Route("GetAll")]
        //[Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> GetAll() 
        {
            var query = new GetAllUserQuery();
            var res = await _mediator.Send(query);
            return Ok(res);
        }
        [HttpGet]
        [Route("GetById")]
        //[Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> GetById(long productId)
        {
            var query = new GetByIdQuery();
            query.ProductId = productId;
            var res = await _mediator.Send(query);
            return Ok(res);
        }

        [HttpPost]
        [Route("Add")]
        //[Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> Add(ProductCommand command)
        {
            var res = await _mediator.Send(command);
            return Ok(res);
        }


    }
}
