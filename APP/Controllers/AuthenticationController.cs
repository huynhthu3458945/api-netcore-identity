﻿using APP.DbContext.Indentity;
using APP.DbContext.Models.Utils;
using APP.Services.Interfaces;
using APP.WebApi.Models.Response;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace APP.WebApi.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        public AuthenticationController(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
        }
        [HttpPost]
        [Route("Register")]
        public async Task<ResponseResult<string>> Register(RegisterRequest request)
        {
            var userExist = await _userManager.FindByNameAsync(request.UserName);
            if (userExist != null)
                return new ResponseResult<string>(RetCodeEnum.ApiError, "Tài khoản đã tồn tài", null);
            AppUser appUser = new AppUser()
            {
                Email = request.Email,
                UserName = request.UserName,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            var res = await _userManager.CreateAsync(appUser, request.Password);
            if (!res.Succeeded)
            {
                return new ResponseResult<string>(RetCodeEnum.ApiError, String.Join(" ", res.Errors.ToList().Select(z => z.Description)), null);
            }
            await _userManager.AddToRoleAsync(appUser, "Member");
            return new ResponseResult<string>(RetCodeEnum.Ok, "Đã thêm thành công", null);
        }
        [HttpPost]
        [Route("Login")]
        public async Task<ResponseResult<UserResponse>> Login(LoginRequest request)
        {
            var user = await _userManager.FindByNameAsync(request.UserName);
            if (user != null)
            {
                if (await _userManager.CheckPasswordAsync(user, request.Password) != null)
                {
                    var userRole = await _userManager.GetRolesAsync(user);
                    var authClaims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Sid, user.Id),
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(ClaimTypes.Email, user.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    };
                    foreach(var role in userRole)
                    {
                        authClaims.Add(new Claim(ClaimTypes.Role, role));
                    }
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    var token = new JwtSecurityToken(
                                    _configuration["JWT:Issuer"],
                                    _configuration["JWT:Audience"],
                                    authClaims,
                                    expires: DateTime.Now.AddHours(5),
                                    signingCredentials: creds);

                    var tokenString = (new JwtSecurityTokenHandler().WriteToken(token));

                    var userResponse = new UserResponse();
                    userResponse.UserName = user.UserName;
                    userResponse.Email = user.Email;
                    userResponse.Token = tokenString;
                    return new ResponseResult<UserResponse>(RetCodeEnum.Ok, "Đăng nhập thành công", userResponse);
                }
                return new ResponseResult<UserResponse>(RetCodeEnum.ApiError, "Mật khẩu không đúng", null);
            }
            return new ResponseResult<UserResponse>(RetCodeEnum.ApiError, "Lỗi hệ thống", null);
        }
    }
}
