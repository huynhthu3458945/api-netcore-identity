﻿namespace APP.WebApi.Common
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string Manager = "Manager";
        public const string Member = "Member";
    }
}
