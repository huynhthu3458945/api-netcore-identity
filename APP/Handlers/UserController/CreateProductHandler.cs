﻿using APP.DbContext.Models;
using APP.Services.Interfaces;
using APP.WebApi.Models.Request;
using APP.WebApi.Queries.UserController;
using MediatR;

namespace APP.WebApi.Handlers.UserController
{
    public class CreateProductHandler : IRequestHandler<ProductCommand, Product>
    {
        private readonly IProductService _productService;
        public CreateProductHandler(IProductService productService)
        {
            _productService = productService;
        }
        public async Task<Product> Handle(ProductCommand request, CancellationToken cancellationToken)
        {
            var product = new Product()
            {
                BrandId = request.BrandId,
                ProductName = request.ProductName,
                Price = request.Price,
                CategoryId = request.CategoryId
            };
            var pro = await _productService.AddAsync(product);
            return pro;
        }
    }
}
