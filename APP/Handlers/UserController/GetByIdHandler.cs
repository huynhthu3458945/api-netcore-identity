﻿using APP.DbContext.Models;
using APP.Services.Cache;
using APP.Services.Interfaces;
using APP.Services.Logger;
using APP.WebApi.Queries.UserController;
using Exceptions;
using MediatR;
using System.Reflection;

namespace APP.WebApi.Handlers.UserController
{
    public class GetByIdHandler : IRequestHandler<GetByIdQuery, Product>
    {
        private readonly IProductService _productService;
        private readonly ILoggerService _logger;

        public GetByIdHandler(IProductService productService, ILoggerService logger)
        {
            _productService = productService;
            _logger = logger;
        }
        public async Task<Product> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                _logger.WriteInfoLog($"{nameof(GetByIdHandler)}: {request.ProductId}");
                var pro = await _productService.GetById(request.ProductId);
                return pro;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, functionName: nameof(GetByIdHandler));
                throw new CustomerException(ex.Message);
                return null;
            }
        }
    }
}
