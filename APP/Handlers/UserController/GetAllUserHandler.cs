﻿using APP.DbContext.Models;
using APP.Services.Cache;
using APP.Services.Interfaces;
using APP.Services.Logger;
using APP.WebApi.Queries.UserController;
using Exceptions;
using MediatR;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace APP.WebApi.Handlers.UserController
{
    public class GetAllUserHandler : IRequestHandler<GetAllUserQuery, IEnumerable<Product>>
    {
        private readonly IProductService _productService;
        private readonly ILoggerService _logger;
        private readonly IMemoryCacheService _memoryCacheService;


        public GetAllUserHandler(IProductService productService, ILoggerService logger, IMemoryCacheService memoryCacheService)
        {
            _productService = productService;
            _logger = logger;
            _memoryCacheService = memoryCacheService;
        }
        public async Task<IEnumerable<Product>> Handle(GetAllUserQuery request, CancellationToken cancellationToken)
        {
            try
            {
                //throw new NotImplementedException();
                var pro = new List<Product>();
                string keyCachePro = "Cache_GetAllProduct";
                if (_memoryCacheService.TryGetValue<IEnumerable<Product>>(keyCachePro, out var cachePro))
                {
                    pro = cachePro.ToList();
                }
                else
                {
                    _logger.WriteInfoLog("Call GetAll Product");
                    pro = (await _productService.GetAll()).ToList();
                    _memoryCacheService.SetValueToCache(keyCachePro, pro, 1);
                }
                return pro;
            }
            catch (Exception ex) {
                throw new CustomerException("Lỗi tao tự tạo");
                return null;
            }
        }
    }
}
