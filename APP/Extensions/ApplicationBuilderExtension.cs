﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using System.Text.Json;
using System.Text;
using Exceptions;

namespace APP.WebApi.Extensions
{
    public static class ApplicationBuilderExtension
    {
        public static void UseFluentValidationExceptionHandler(this IApplicationBuilder app)
        {
            // Handler lỗi
            app.UseExceptionHandler(x =>
            {
                x.Run(
                    async context =>
                    {
                        var erroFeature = context.Features.Get<IExceptionHandlerFeature>();
                        var exception = erroFeature.Error;
                        if (exception is ValidationException validationException)
                        {
                            var errors = validationException.Errors.Select(err => new
                            {
                                err.PropertyName,
                                err.ErrorMessage
                            });
                            var errorText = JsonSerializer.Serialize(errors);
                            context.Response.StatusCode = 400;
                            context.Response.ContentType = "application/json";
                            await context.Response.WriteAsync(errorText, Encoding.UTF8);
                        }
                        else if (exception is CustomerException customerException)
                        {
                            var errors = new {
                                customerException.Message
                            } ;
                            var errorText = JsonSerializer.Serialize(errors);
                            context.Response.StatusCode = 401;
                            context.Response.ContentType = "application/json";
                            await context.Response.WriteAsync(errorText, Encoding.UTF8);
                        }
                        else
                            throw exception;


                    });
            });
        }
    }
}
