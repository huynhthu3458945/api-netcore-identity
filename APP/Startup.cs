﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.OpenApi.Models;
using Serilog;
using APP.DbContext;
using APP.DbContext.Indentity;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Mvc.Razor;
using APP.Services.Interfaces;
using APP.Services.Implements;
using APP.WebApi.Common;
using MediatR;
using FluentValidation;
using APP.WebApi.PipelineBehaviors;
using APP.WebApi.Extensions;
using APP.Services.Implements.Cached;
using APP.Services.Logger;
using APP.Services.Cache;

namespace APP
{
    public class Startup
    {

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var mainConnectString = Configuration["MainConnectionString"];
            services.AddControllers();

            // EntityFramework DB
            services.ConfigureDbContext(mainConnectString);

            // Config Token
            var issuer = Configuration.GetValue<string>("JWT:Issuer");
            var audience = Configuration.GetValue<string>("JWT:Audience");
            var signingKey = Configuration.GetValue<string>("JWT:Key");
            var signingKeyBytes = System.Text.Encoding.UTF8.GetBytes(signingKey);

            // Add Authen
            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
             // Bearer
             .AddJwtBearer(opt =>
             {
                 opt.SaveToken = true;
                 opt.RequireHttpsMetadata = false;
                 opt.TokenValidationParameters = new TokenValidationParameters()
                 {
                     ValidateIssuer = true,
                     ValidIssuer = issuer,
                     ValidateAudience = true,
                     ValidAudience = audience,
                     IssuerSigningKey = new SymmetricSecurityKey(signingKeyBytes)
                 };
             });

            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(
                swa =>
                {
                    var securitySchema = new OpenApiSecurityScheme
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.Http,
                        Scheme = JwtBearerDefaults.AuthenticationScheme,
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = JwtBearerDefaults.AuthenticationScheme
                        }
                    };
                    swa.AddSecurityDefinition("Bearer", securitySchema);

                    var securityRequirement = new OpenApiSecurityRequirement
                {
                    { securitySchema, new[] { "Bearer" } }
                };
                    swa.AddSecurityRequirement(securityRequirement);
                }
                );

            services.AddMemoryCache();
            services.AddScoped<IMemoryCacheService, MemoryCacheService>();

            services.AddSingleton<ILoggerService, LoggerService>();
            services.AddScoped<IProductService, ProductService>();
            services.Decorate<IProductService, CachedProductService>();

            // MediatR and FluentValidation
            services.AddMediatR(typeof(Startup));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddValidatorsFromAssembly(typeof(Startup).Assembly);

        }

        public async void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseFluentValidationExceptionHandler();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Application");
            });
            app.UseHttpsRedirection();
            app.UseCors();
            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            loggerFactory.AddSerilog();

            CreateRoleAndUser(app);
        }

        private async void CreateRoleAndUser(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var roles = new[] { UserRoles.Admin, UserRoles.Manager, UserRoles.Manager };

                foreach (var role in roles)
                {
                    if (!await roleManager.RoleExistsAsync(role))
                    {
                        await roleManager.CreateAsync(new IdentityRole(role));
                    }
                }
            }
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                string email = "admin@gmail.com";
                string passWd = "Test123,";
                if (await userManager.FindByNameAsync(email) == null)
                {
                    var users = new AppUser()
                    {
                        Email = email,
                        UserName = email
                    };
                    var res = await userManager.CreateAsync(users, passWd);
                    var res1 = await userManager.AddToRoleAsync(users, "Admin");
                }
            }
        }
    }
}
