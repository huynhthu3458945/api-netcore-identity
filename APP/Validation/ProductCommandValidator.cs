﻿using APP.WebApi.Models.Request;
using FluentValidation;

namespace APP.WebApi.Validation
{
    public class ProductCommandValidator : AbstractValidator<ProductCommand>
    {
        public ProductCommandValidator()
        {
               RuleFor(_=>_.CategoryId).NotNull().WithMessage("Không đc null").NotEmpty().WithMessage("Không đc trống");
               RuleFor(_=>_.BrandId).NotNull().NotEmpty();
               RuleFor(_=>_.ProductName).NotNull().NotEmpty();
        }
    }
}
