﻿using APP.WebApi.Models.Request;
using APP.WebApi.Queries.UserController;
using FluentValidation;

namespace APP.WebApi.Validation
{
    public class UserGetByIdValidator : AbstractValidator<GetByIdQuery>
    {
        public UserGetByIdValidator()
        {
            RuleFor(_ => _.ProductId).NotNull().WithMessage("Không đc null").NotEmpty().WithMessage("Không đc trống");
        }
    }
}
