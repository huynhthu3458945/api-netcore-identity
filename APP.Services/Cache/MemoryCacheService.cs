﻿using APP.Services.Logger;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.Services.Cache
{
    public class MemoryCacheService : IMemoryCacheService
    {
        private readonly ILoggerService _logger;
        private readonly IMemoryCache _memoryCache;

        public MemoryCacheService(ILoggerService logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _memoryCache = memoryCache;
        }

        public bool TryGetValue<T>(string key, out T outputValue)
        {
            _logger.WriteInfoLog($"Call TryGetValue: {key}");
            return _memoryCache.TryGetValue<T>(key, out outputValue);
        }

        public void SetValueToCache<T>(string key, T value, int expMintues)
        {
            var options = new MemoryCacheEntryOptions();
            options.AbsoluteExpiration = DateTime.Now.AddMinutes(expMintues);
            if (_memoryCache.TryGetValue<string>(key, out var valueOutput))
            {
                _memoryCache.Remove(key);
            }

            _logger.WriteInfoLog($"Call SetValueToCache: {key}");
            _memoryCache.Set(key, value, options);
        }

        public void RemoveValueFromCache(string key)
        {
            _logger.WriteInfoLog($"Call  RemoveValueFromCache: {key}");
            if (_memoryCache.TryGetValue<string>(key, out var valueOutput))
            {
                _memoryCache.Remove(key);
            }
        }
    }
}
