﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.Services.Cache
{
    public interface IMemoryCacheService
    {
        bool TryGetValue<T>(string key, out T outputValue);
        void SetValueToCache<T>(string key, T value, int expMintues);
        void RemoveValueFromCache(string key);
    }
}
