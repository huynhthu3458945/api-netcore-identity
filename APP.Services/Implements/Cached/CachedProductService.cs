﻿using APP.DbContext.Models;
using APP.Services.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.Services.Implements.Cached
{
    public class CachedProductService : IProductService
    {
        private readonly IProductService _productService;
        private readonly ConcurrentDictionary<string, IEnumerable<Product>> _cache = new ConcurrentDictionary<string, IEnumerable<Product>>();    
        public CachedProductService(IProductService productService)
        {
            _productService = productService;
        }
        public async Task<Product> AddAsync(Product product)
        {
            var pro = await _productService.AddAsync(product);
            return pro;
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            string key = "GetAll";
            if (_cache.ContainsKey(key))
                return _cache[key];
            var pro = await _productService.GetAll();
            _cache.TryAdd(key, pro);
            return pro;
        }

        public Task<Product> GetById(long productId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Category>> GetCategoryAll()
        {
            throw new NotImplementedException("aaa");
        }
    }
}
