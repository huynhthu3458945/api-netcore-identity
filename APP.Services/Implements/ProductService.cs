﻿using APP.DbContext;
using APP.DbContext.Common;
using APP.DbContext.Models;
using APP.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.Services.Implements
{
    public class ProductService : IProductService
    {
        //private readonly ILoggerService _logger;
        private readonly IGenericDbContext<AppDbContext> _unitOfWork;
        public ProductService(IGenericDbContext<AppDbContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<Product>> GetAll()
        {
            return await _unitOfWork.Repository<Product>().AsNoTracking().ToListAsync();
        }
        public async Task<Product> GetById(long productId)
        {
            return await _unitOfWork.Repository<Product>().AsNoTracking().FirstOrDefaultAsync(z => z.ProductId == productId);
        }
        public async Task<IEnumerable<Category>> GetCategoryAll()
        {
            return await _unitOfWork.Repository<Category>().AsNoTracking().ToListAsync();
        }
        public async Task<Product> AddAsync(Product product)
        {
            await _unitOfWork.Repository<Product>().AddAsync(product);
            await _unitOfWork.SaveChangesAsync();
            return product;
        }
    }

}
