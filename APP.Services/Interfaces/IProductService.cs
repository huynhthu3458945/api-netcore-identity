﻿using APP.DbContext.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.Services.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAll();
        Task<Product> GetById(long productId);
        Task<IEnumerable<Category>> GetCategoryAll();
        Task<Product> AddAsync(Product product);
    }
}
