add-migration -Configuration MyFirstApp.Migrations.Configuration Name
update-database -Configuration MyFirstApp.Migrations.Configuration 

add-migration -Configuration MyFirstApp.IdentityMigrations.Configuration Name
update-database -Configuration MyFirstApp.IdentityMigrations.Configuration 


-- Core

add-migration -Context AppDbContext
update-database -Context AppDbContext

 -- Core - Identity
add-migration -Context AppIdentityDbContext
update-database -Context AppIdentityDbContext