﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext.Models.Utils
{
    public class LoginRequest
    {
        [Required(ErrorMessage = "Tài không không được bỏ trống")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password không được bỏ trống")]
        public string Password { get; set; }
    }
}
