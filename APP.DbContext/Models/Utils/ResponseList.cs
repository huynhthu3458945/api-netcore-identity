﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext.Models.Utils
{
    public class ResponseList
    {
        public Paging Paging { get; set; }
        public Object ListData { get; set; }
    }

    public class ResponseCombo
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class ResponseAffiliateList
    {
        public Paging Paging { get; set; }
        public Object ExtendsData { get; set; }
        public Object ListData { get; set; }
    }
}
