﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext.Models
{
    public class OrderDetail
    {
        [Key, Column(Order = 1)]
        public Guid OrderId { get; set; }
        public virtual Order Order { get; set; }
        [Key, Column(Order = 2)]
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }

        public Nullable<decimal> Price { get; set; }
    }
}
