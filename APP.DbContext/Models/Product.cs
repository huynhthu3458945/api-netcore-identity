﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace APP.DbContext.Models
{
    public class Product
    {
        [Key]
        public long ProductId { get; set; }
        [Display(Name = "Tên sản phẩm")]
        [Required(ErrorMessage = "Vui lòng nhập tên sản phẩm!")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập giá!")]
        //[LonHon10(ErrorMessage = "Giá phải lớn hơn 10")]
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> PriceCost { get; set; }
        public Nullable<decimal> PricePro { get; set; }
        public long BrandId { get; set; }
        public virtual Brand Brand { get; }
        public long CategoryId { get; set; }
        public virtual Category Category { get; }
    }
}
