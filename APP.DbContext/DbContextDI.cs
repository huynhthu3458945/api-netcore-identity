﻿using APP.DbContext.Base;
using APP.DbContext.Common;
using APP.DbContext.Indentity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext
{
    public static class DbContextDI
    {
        public static IServiceCollection ConfigureDbContext(this IServiceCollection services, string connectString)
        {
            services.AddScoped<IContext, AppDbContext>();
            services.AddScoped(typeof(IGenericDbContext<>), typeof(GenericDbContext<>));
            services.AddDbContext<AppIdentityDbContext>(options => options.UseSqlServer(connectString, o => o.CommandTimeout(180)));
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connectString, o => o.CommandTimeout(180)));

            // For Identity
            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<AppIdentityDbContext>()
                .AddDefaultTokenProviders();

            return services;
        }
    }
}
