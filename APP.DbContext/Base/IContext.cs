﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext.Base
{
    public interface IContext : IDisposable
    {
        //T CreateDbContext<T>(IOptions<ConnectStringsSetting> connectOps) where T : class;
        DbSet<T> Repository<T>() where T : class;
        int SaveChange();
        Task<int> SaveChangeAsync();
    }
}
