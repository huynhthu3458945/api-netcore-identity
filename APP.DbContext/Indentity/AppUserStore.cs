﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext.Indentity
{
    public class AppUserStore : UserStore<AppUser>
    {
        public AppUserStore(AppIdentityDbContext appDbContext) : base(appDbContext)
        {

        }
    }
}
