﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext.Indentity
{
    public class AppUser : IdentityUser
    {
        public DateTime? Birthday { get; set; }
    }
}
