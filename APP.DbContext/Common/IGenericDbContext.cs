﻿using APP.DbContext.Base;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.DbContext.Common
{
    public interface IGenericDbContext<T> where T : Microsoft.EntityFrameworkCore.DbContext, IContext, IDisposable
    {
        DatabaseFacade Database { get; }
        DbSet<T> Repository<T>() where T : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void Dispose();
        T GetContext();
    }
}
